export class PontosTurísticos {
  constructor() {
    this.places = JSON.parse(localStorage.getItem("places")) || [
      {
        description:
          "Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.",
        image: "/images/card1-image.webp",
        name: "Pão de Açúcar",
      },
      {
        description:
          "Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.",
        image: "/images/card2-image.webp",
        name: "Ilha Grande",
      },
      {
        description:
          "Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.",
        image: "/images/card3-image.webp",
        name: "Cristo Redentor",
      },
      {
        description:
          "Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.",
        image: "/images/card4-image.webp",
        name: "Centro Histórico de Paraty",
      },
    ];

    this.selectors();
    this.events();
  }

  selectors() {
    this.imageInput = document.querySelector(".item-image-input");
    this.imageInputWrapper = document.querySelector(".item-image-wrapper");
    this.itemsContainer = document.querySelector(".items-container");
    this.itemTitleInput = document.querySelector(".item-title");
    this.itemDescriptionInput = document.querySelector(".item-description");
    this.addItemForm = document.querySelector(".add-item-form");
  }

  events() {
    window.addEventListener("load", this.renderItems(this.places));
    this.imageInput.addEventListener(
      "change",
      this.displayImagePreview.bind(this)
    );
    this.addItemForm.addEventListener("submit", (e) => {
      this.addItem(e);
    });
  }

  displayImagePreview() {
    const image = this.imageInput.files[0];

    const fileReader = new FileReader();
    fileReader.readAsDataURL(image);
    fileReader.onload = () => {
      const imagePreview = document.createElement("img");
      imagePreview.setAttribute("src", fileReader.result);
      imagePreview.classList.add("item-image-preview");
      const deleteButton = document.createElement("div");
      deleteButton.classList.add("delete-image-button");
      this.imageInputWrapper.append(imagePreview, deleteButton);

      deleteButton.addEventListener(
        "click",
        this.deleteImagePreview.bind(this)
      );
    };
  }

  renderItems(arr) {
    arr.forEach((elem) => {
      const item = this.createItem(elem);

      this.itemsContainer.appendChild(item);
    });
  }

  createItem(elem) {
    const item = document.createElement("div");
    item.classList.add("item-card");

    item.innerHTML = `
        <img
        src=${elem.image}
        alt="item-image"
        class="item-card-image"
      />
      <div class="item-card-info">
        <h3 class="item-card-title">${elem.name}</h3>
  
        <p class="item-card-description">
          ${elem.description}
        </p>
      </div>
    `;
    return item;
  }

  addItem(e) {
    e.preventDefault();
    const image = this.imageInput.files[0];
    const name = this.itemTitleInput.value;
    const description = this.itemDescriptionInput.value;

    const fileReader = new FileReader();
    fileReader.readAsDataURL(image);

    fileReader.onload = () => {
      const item = {
        image: fileReader.result,
        name: name,
        description: description,
      };
      this.places.push(item);

      const itemDOM = this.createItem(this.places[this.places.length - 1]);
      this.itemsContainer.appendChild(itemDOM);

      this.reset();

      localStorage.setItem("places", JSON.stringify(this.places));
    };
  }

  reset() {
    this.itemTitleInput.value = "";
    this.itemDescriptionInput.value = "";

    this.deleteImagePreview();
  }

  deleteImagePreview() {
    const imagePreview = document.querySelector(".item-image-preview");
    const deleteButton = document.querySelector(".delete-image-button");
    this.imageInput.value = "";
    this.imageInputWrapper.removeChild(imagePreview);
    this.imageInputWrapper.removeChild(deleteButton);
  }
}
